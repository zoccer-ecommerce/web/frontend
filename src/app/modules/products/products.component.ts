import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';

import ProductFacade from '@app/core/facades/product.facade';
import { Product } from '@app/shared/models/product';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})
export class ProductsComponent implements OnInit, OnDestroy {

  constructor(public productFacade: ProductFacade) { }

  public retrieveProductsSubscription!: Subscription;
  public products!: Product[];

  ngOnInit(): void {
    this.retrieveProductsSubscription = this.productFacade.products$.subscribe((result) => {
      this.products = result;
      // console.log(this.products);
    });
  }

  ngOnDestroy(): void {
    this.retrieveProductsSubscription.unsubscribe();
  }

}
