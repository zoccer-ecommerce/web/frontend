import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ProductsComponent } from './products.component';
import { ProductListComponent } from './components/product-list/product-list.component';
import { ProductDetailComponent } from './components/product-detail/product-detail.component';

const routes: Routes = [
  { 
    path: '', 
    component: ProductsComponent,
    children: [
      {
        path: 'all', component: ProductListComponent,
      },
      {
        path: ':id', component: ProductDetailComponent,
      },
      {
        path: '',
        redirectTo: 'all',
        pathMatch: 'full',
      }
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProductsRoutingModule { }
