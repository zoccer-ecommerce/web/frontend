import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';

import ProductFacade from '@app/core/facades/product.facade';
import { Product } from '@app/shared/models/product';

@Component({
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.scss']
})
export class ProductListComponent implements OnInit, OnDestroy {

  constructor(public productFacade: ProductFacade) { }

  public retrieveProductsSubscription!: Subscription;
  public products!: Product[];

  ngOnInit(): void {
    this.retrieveProductsSubscription = this.productFacade.products$.subscribe((result) => {
      this.products = result;
      // console.log(this.products);
    });
  }

  public calculateNetPrice(price: number, discount: number): string {
    return `${price - discount}`;
  }

  public addToCart(productId: number) {
    console.log('Start Adding to cart...', productId);
  }

  ngOnDestroy(): void {
    this.retrieveProductsSubscription.unsubscribe();
  }

}
