import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';

import ProductFacade from '@app/core/facades/product.facade';
import { Product } from '@app/shared/models/product';

@Component({
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.scss']
})
export class ProductDetailComponent implements OnInit, OnDestroy {

  constructor(
    private route: ActivatedRoute,
    public productFacade: ProductFacade
  ) { }

  public routeParamSubscription!: Subscription;
  public retrieveProductDetailSub!: Subscription;
  public routeIdParam!: string | null;
  public product: Product | null = null;

  ngOnInit(): void {
    this.routeParamSubscription = this.route.params.subscribe((params) => {
      this.routeIdParam = params['id'];
      
      this.productFacade.retrieveProductDetail(Number(this.routeIdParam));
    });

    this.retrieveProductDetailSub = this.productFacade.product$
      .subscribe((product: Product) => {
        this.product = product;
        // console.log('Product =>', product);
      });
  }

  public calculateNetPrice(price: number, discount: number): string {
    return `${price - discount}`;
  }

  public addToCart(productId: number) {
    console.log('Start Adding to cart...', productId);
  }

  ngOnDestroy(): void {
    this.routeParamSubscription.unsubscribe();
    this.retrieveProductDetailSub.unsubscribe();
  }

}
