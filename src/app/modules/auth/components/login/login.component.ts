import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from '@app/core/services/auth/auth-service.service';
import { RestApiResponse } from '@app/core/services/rest-api.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit, OnDestroy {
  public myForm!: FormGroup;
  public loginSubscription!: Subscription;
  public serverErrorMessage: string = '';
  public isLoggingIn: boolean = false;

  constructor(
    private authService: AuthService,
    public formBuilder: FormBuilder,
    public router: Router,
  ) { }

  ngOnInit(): void {
    this.reactiveForm();
  }

  /* reactive form */
  public reactiveForm() {
    this.myForm = this.formBuilder.group({
      username: ['', [Validators.required]],
      password: ['', [Validators.required]], // TODO: password length validation
    })
  }

  // TODO: add to a central file, and reuse in signup component
  public errorHandling = (controlName: string, errorName: string) => {
    return this.myForm.get(controlName)?.hasError(errorName);
  }

  public login() {
    this.serverErrorMessage = '';

    if (this.myForm.valid) {
      // console.log("Form values ->", this.myForm.value);
      // TODO: trimming user input values before sending to backend
      this.isLoggingIn = true;

      this.loginSubscription = this.authService.login$(this.myForm.value)
        .subscribe(
          (result) => {
            // console.log("Login Result =>", result);

            this.authService.saveAuthToken({
              accessToken: result.data.accessToken,
              refreshToken: result.data.refreshToken
            });

            this.router.navigate(['home']);
            this.isLoggingIn = false;
          },
          (error: RestApiResponse) => {
            this.serverErrorMessage = error.message;
            this.isLoggingIn = false;
          }
        );

    }
  }

  ngOnDestroy(): void {
    this.loginSubscription && this.loginSubscription.unsubscribe();
  }

}
