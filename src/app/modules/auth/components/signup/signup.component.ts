import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from '@app/core/services/auth/auth-service.service';
import { RestApiResponse } from '@app/core/services/rest-api.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit, OnDestroy {
  public myForm!: FormGroup;
  public signupSubscription!: Subscription;
  public serverErrorMessage: string = '';
  public isSigningUp: boolean = false;

  constructor(
    private authService: AuthService,
    public formBuilder: FormBuilder,
    public router: Router,
  ) { }

  ngOnInit(): void {
    this.reactiveForm();
  }

  /* reactive form */
  public reactiveForm() {
    this.myForm = this.formBuilder.group({
      username: ['', [Validators.required]], // TODO: validation - should not contain spaces
      password: ['', [Validators.required]], // TODO: password length validation
      email: ['', [Validators.required]], // TODO: email validation
      phoneNumber: ['', [Validators.required]], // TODO: phone number validation
    })
  }

  public errorHandling = (controlName: string, errorName: string) => {
    return this.myForm.get(controlName)?.hasError(errorName);
  }

  public signup() {
    this.serverErrorMessage = '';

    if (this.myForm.valid) {
      // console.log("Form values ->", this.myForm.value);
      // TODO: trimming user input values before sending to backend

      this.isSigningUp = true;

      this.signupSubscription = this.authService.signup$(this.myForm.value)
        .subscribe(
          (result) => {
            // console.log("Signup Result =>", result);

            this.router.navigate(['auth/login']);
            this.isSigningUp = false;
          },
          (error: RestApiResponse) => {
            this.serverErrorMessage = error.message;
            this.isSigningUp = false;
          }
        );
    }
  }

  ngOnDestroy(): void {
    this.signupSubscription && this.signupSubscription.unsubscribe();
  }

}
