import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '@app/core/services/auth/auth-service.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.scss']
})
export class NavComponent implements OnInit, OnDestroy {

  constructor(
    private authService: AuthService,
    public router: Router
  ) { }

  logoutSubcsription!: Subscription;
  isLoggedInSubcsription!: Subscription;
  isLoggedIn: boolean = false;

  ngOnInit(): void {
    this.isLoggedInSubcsription = this.authService.isLoggedIn$
      .subscribe((currentLoginStatus: boolean) => {
        this.isLoggedIn = currentLoginStatus;
      });
  }

  public routeToAuthPage(authType: 'login' | 'signup', sidenav?: any) {
    sidenav && sidenav.close();
    this.router.navigate([`auth/${authType}`]);
  }

  public routeToHomePage(sidenav?: any) {
    sidenav && sidenav.close();
    this.router.navigate([`home`]);
  }

  public routeToCartPage(sidenav?: any) {
    sidenav && sidenav.close();
    this.router.navigate([`cart`]);
  }

  public logout(sidenav?: any): void {
    const authToken = this.authService.getAuthToken();
    const logoutPayload = { refreshToken: authToken.refreshToken as string };

    this.logoutSubcsription = this.authService.logout$(logoutPayload)
      .subscribe(
        (result) => {
          this.authService.removeAuthToken();
          sidenav && sidenav.close();
          this.router.navigate(['home']);
        },
        (error) => {
          // TODO: display the error to the user
          console.log('Error during logout =>', error);
        }
      );
  }

  ngOnDestroy(): void {
    this.isLoggedInSubcsription.unsubscribe();
    this.logoutSubcsription && this.logoutSubcsription.unsubscribe();
  }

}
