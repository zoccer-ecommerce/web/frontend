export interface ProductCategory {
  id: number;
  name: string;
  createdAt: string;
  updatedAt: string;
}

export interface Product {
  id: number;
  name: string;
  price: number;
  discountPrice: number;
  stock: number;
  description: string;
  image: string;
  salesCount: number;
  createdAt: string;
  updatedAt: string;
  category: ProductCategory;
}
