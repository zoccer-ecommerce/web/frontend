import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from './material.module';
import { RouterModule } from '@angular/router';
import { FlexLayoutModule } from '@angular/flex-layout';

@NgModule({
  declarations: [
    /* our own custom components */
  ],
  imports: [
    /* angular stuff */
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,

    /* 3rd party modules */
    MaterialModule
  ],
  exports: [
    /* angular stuff */
    CommonModule,
    FormsModule,
    FlexLayoutModule,
    ReactiveFormsModule,
    RouterModule,

    /* 3rd party modules */
    MaterialModule,

    /* our own custom components */

  ]
})
export class SharedModule { }
