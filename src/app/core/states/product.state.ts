import { Injectable } from '@angular/core';
import { BehaviorSubject, Subject } from 'rxjs';

import { Product } from '../../shared/models/product';
import { ProductService } from '../services/products/product.service';

@Injectable({ providedIn: 'root' })
export default class ProductState {
  private readonly _products$ = new BehaviorSubject<Product[]>([]);
  private readonly _product$ = new Subject<Product>();
  private readonly _activeProduct$ = new Subject<Product>();

  public products$ = this._products$.asObservable();
  public product$ = this._product$.asObservable();
  public activeProduct$ = this._activeProduct$.asObservable();

  constructor(private readonly _productService: ProductService) {
    this._productService.retrieveProductList$()
      .subscribe((result: any) => this._products$.next(result.data.products));
  }

  public retrieveProductDetail(productId: number): void {
    // const product = this._products$.value.find((product: Product) => product.id === productId);
    // this._activeProduct$.next(product);

    this._productService.retrieveProductDetail$(productId)
      .subscribe((result: any) => {
        // this._activeProduct$.next(result.data);
        this._product$.next(result.data);
      });
  }
  
}
