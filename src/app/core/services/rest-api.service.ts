import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { environment } from '@env';

// TODO: move in a separate file
export interface RestApiResponse {
  status: string;
  message: string;
  data: any;
}

@Injectable({
  providedIn: 'root'
})
export class RestApiService {
  private BASE_URL = environment.apiUrl;

  constructor(private httpClient: HttpClient) { }

  public httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
    })
  }

  // GET
  public get$<T>(url: string): Observable<T> {
    return this.httpClient.get<T>(`${this.BASE_URL}/${url}/`, this.httpOptions)
      .pipe(
        retry(1),
        catchError(this.handleError)
      )
  }

  // POST
  public post$<T>(url: string, body: object): Observable<T> {
    return this.httpClient.post<T>(`${this.BASE_URL}/${url}/`, body, this.httpOptions)
      .pipe(
        retry(1),
        catchError(this.handleError)
      )
  }

  // DELETE
  public delete$<T>(url: string): Observable<T> {
    return this.httpClient.delete<T>(`${this.BASE_URL}/${url}/`, this.httpOptions)
      .pipe(
        retry(1),
        catchError(this.handleError)
      )
  }

  // TODO: add 'PATCH', 'PUT' and 'DELETE' methods

  // error handling // TODO: better error handling
  public handleError(error: any) {
    let errorMessage = 'Unknown Error';

    if (error.error instanceof ErrorEvent) {
      // client-side error
      errorMessage = error.error.message;
    } else {
      // server-side error
      errorMessage = `Error code: ${error.status}\nMessage: ${error.message}`
    }

    console.log('Error =>', error.error);

    // return throwError(errorMessage);
    return throwError(error.error);
  }
}
