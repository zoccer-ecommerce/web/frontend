import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { RestApiService } from '../rest-api.service';

export interface AuthToken {
  accessToken: string | null;
  refreshToken: string | null;
}

export interface LoginPayload {
  username: string;
  password: string;
}

export interface LogoutPayload {
  refreshToken: string;
}

export interface SignupPayload {
  username: string;
  password: string;
  email: string;
  phoneNumber: string;
}

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  constructor(private restApiService: RestApiService) { }

  private _isLoggedIn$ = new BehaviorSubject<boolean>(this.hasAccessToken());
  public isLoggedIn$ = this._isLoggedIn$.asObservable();

  private _changeLoginStatus(status: boolean): void {
    this._isLoggedIn$.next(status);
  }

  public getAuthToken(): AuthToken {
    const accessToken = localStorage.getItem("accessToken");
    const refreshToken = localStorage.getItem("refreshToken");

    return { accessToken, refreshToken };
  }

  public saveAuthToken({ accessToken, refreshToken }: AuthToken): void {
    localStorage.setItem("accessToken", accessToken as string);
    localStorage.setItem("refreshToken", refreshToken as string);

    this._changeLoginStatus(true);
  }

  public removeAuthToken(): void {
    localStorage.removeItem("accessToken");
    localStorage.removeItem("refreshToken");

    this._changeLoginStatus(false);
  }


  public hasAccessToken(): boolean {
    const authToken = this.getAuthToken();

    if (!authToken.accessToken) {
      return false;
    }

    return true;
  }

  public login$(payload: LoginPayload): Observable<any> {
    return this.restApiService.post$('auth/login', payload);
  }

  public signup$(payload: SignupPayload): Observable<any> {
    return this.restApiService.post$('auth/signup', payload);
  }

  public logout$(payload: LogoutPayload) {
    return this.restApiService.post$('auth/logout', payload);
  }
}
