import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AuthService } from '@app/core/services/auth/auth-service.service';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  constructor(private authService: AuthService) {}

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const authToken = this.authService.getAuthToken();

    // console.log('INTERCEPTING REQUEST...', request);

    if (!authToken.accessToken) {
      return next.handle(request);
    }

    const clonedRequest = request.clone({
      // headers: request.headers.set("Authorization", `Bearer ${authToken.accessToken}`)
      setHeaders: {
        'Authorization': `Bearer ${authToken.accessToken}`,
      }
    });

    // console.log('CLONED REQUEST...', clonedRequest);

    return next.handle(clonedRequest)
  }
}
