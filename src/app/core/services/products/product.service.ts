import { Injectable } from '@angular/core';
import { RestApiService } from '@core/services/rest-api.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  private endpoint = 'products';

  constructor(private restApiService: RestApiService) { }

  public retrieveProductList$<T>(): Observable<T> {
    return this.restApiService.get$<T>(this.endpoint);
  }

  public retrieveProductDetail$<T>(productId: number): Observable<T> {
    return this.restApiService.get$<T>(`${this.endpoint}/${productId}`);
  }
}
