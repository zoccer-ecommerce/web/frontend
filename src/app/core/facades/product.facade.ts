import { Injectable } from '@angular/core';
import ProductState from '../states/product.state';

@Injectable({ providedIn: 'root' })
export default class ProductFacade {
  public products$ = this._state.products$;
  public product$ = this._state.product$;
  public activeProduct$   = this._state.activeProduct$;
  public retrieveProductDetail = this._state.retrieveProductDetail.bind(this._state);

  constructor(
    private readonly _state: ProductState
  ) { }
}
