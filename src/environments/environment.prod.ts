export const environment = {
  production: true,
  apiUrl: 'https://zoccer-ecommerce-prod.herokuapp.com/api/v1'
};
